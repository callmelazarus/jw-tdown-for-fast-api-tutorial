from fastapi import FastAPI, Depends
from fastapi.testclient import TestClient
from jwtdown_fastapi import authentication


class DictionaryAuthenticator(authentication.Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts,
    ):
        return accounts.get_account(username)

    def get_account_getter(
        self,
        accounts: "Accounts" = Depends(),
    ):
        return accounts

    def get_hashed_password(
        self,
        account_data,
    ):
        return account_data["hashed_password"]


dict_auth = DictionaryAuthenticator("secret key")


class Accounts:
    def get_account(self, username):
        return {
            "email": username,
            "age": 30,
            "hashed_password": dict_auth.hash_password("password"),
            "hashed_password_foo": dict_auth.hash_password("password"),
            "foo_password_hashed": dict_auth.hash_password("password"),
            "password": dict_auth.hash_password("password"),
        }


dict_app = FastAPI()
dict_app.include_router(dict_auth.router)


@dict_app.get("/protected")
async def get_protected(account: dict = Depends(dict_auth.get_current_account_data)):
    return account


@dict_app.get("/not_protected")
async def get_protected(account: dict = Depends(dict_auth.try_get_current_account_data)):
    return account


dict_client = TestClient(dict_app)
